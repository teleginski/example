﻿using System;
using MediatR;

namespace Example.Commands
{
    public class CustomerCommand : IRequest<string>
    {
        public readonly int customerId;

        public CustomerCommand(int id)
        {
            customerId = id;
        }
    }
}
