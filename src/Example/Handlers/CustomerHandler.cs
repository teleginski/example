﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Example.Commands;
using MediatR;

namespace Example.Handlers
{
    public class CustomerHandler : IRequestHandler<CustomerCommand, string>
    {
        public async Task<string> Handle(CustomerCommand request, CancellationToken cancellationToken)
        {
            if (request.customerId == 1)
                return await Task.FromResult("true");
            else
                return await Task.FromResult("false");
        }
    }
}
