using Example.Controllers;
using NUnit.Framework;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Moq;
using System.Threading.Tasks;
using System.Threading;
using Example.Handlers;
using Example.Commands;

namespace Example.Test
{
    public class Tests
    {
        readonly Mock<IMediator> _mediator;

        public Tests()
        {
            _mediator = new Mock<IMediator>();
        }

        [Test]
        public async Task Test_True()
        {
            _mediator.Setup(mock => mock.Send(It.IsAny<IRequest<string>>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult("true"));
            var valuesController = new ValuesController(_mediator.Object);
            string result = await valuesController.Get(1);
            Assert.AreEqual("true", result);
        }

        [Test]
        public async Task Test_False()
        {
            _mediator.Setup(mock => mock.Send(It.IsAny<IRequest<string>>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult("false"));
            var valuesController = new ValuesController(_mediator.Object);
            string result = await valuesController.Get(0);
            Assert.AreEqual("false", result);
        }

        [Test]
        public void Get()
        {
            var data = new string[] { "value1", "value2" };
            var valuesController = new ValuesController(_mediator.Object);
            System.Collections.Generic.IEnumerable<string> result = valuesController.Get();
            Assert.AreEqual(data, result);
        }

        [Test]
        public async Task CustomerHandler_True()
        {
            var customerHandler = new CustomerHandler();

            var customerCommand = new CustomerCommand(1);

            var result = await customerHandler.Handle(customerCommand, default);

            Assert.AreEqual("true", result);
        }

        [Test]
        public async Task CustomerHandler_False()
        {
            var customerHandler = new CustomerHandler();

            var customerCommand = new CustomerCommand(0);

            var result = await customerHandler.Handle(customerCommand, default);

            Assert.AreEqual("false", result);
        }
    }
}